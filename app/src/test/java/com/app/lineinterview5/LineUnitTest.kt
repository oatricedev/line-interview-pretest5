package com.app.lineinterview5

import android.app.Activity
import com.app.lineinterview5.model.DataResponse
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.eq
import com.toyota.toyota.repository.RepositoryContract
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class LineUnitTest {

    private val DATA_RESPONSE = DataResponse()

    @Mock
    private lateinit var repositoryContract: RepositoryContract

    @Mock
    private lateinit var mainView: MainContract.View

    @Mock
    private val contextMock: Activity? = null

    private lateinit var mainPresenter: MainPresenter

    @Before
    fun setupMainPresenter() {
        MockitoAnnotations.initMocks(this)
        mainPresenter = MainPresenter(mainView, repositoryContract)
    }

    @Test
    fun downloadData_showResponseData() {

        mainPresenter.onDownloadInitialData(contextMock!!)

        // Callback is captured and invoked with stubbed notes
        argumentCaptor<RepositoryContract.MainCallback>().apply {

            verify(mainView).showLoading()

            verify(repositoryContract).downloadData(
                    eq(contextMock),
                    capture()
            )

            this.firstValue.responseSuccess(DATA_RESPONSE)

            verify(mainView).dismissLoading()

            verify(mainView).showResponseSuccess(DATA_RESPONSE)

        }
    }

    @Test
    fun downloadData_loading() {

        mainPresenter.onDownloadInitialData(contextMock!!)

        val inOrder = Mockito.inOrder(mainView)
        inOrder.verify(mainView).showLoading()
        inOrder.verify(mainView).dismissLoading()
    }
}
