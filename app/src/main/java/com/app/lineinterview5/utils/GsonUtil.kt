package com.app.lineinterview5.utils

import android.content.Context
import com.google.gson.Gson
import java.io.BufferedReader
import java.io.InputStreamReader

val gson = Gson()

inline fun <reified T> Context.getJson(jsonFileRes: Int): T {
    val inputStream = this.resources.openRawResource(jsonFileRes)
    val bufferReader = BufferedReader(InputStreamReader(inputStream))
    return gson.fromJson(bufferReader, T::class.java)
}