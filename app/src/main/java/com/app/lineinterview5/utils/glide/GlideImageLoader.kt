package com.app.lineinterview5.utils.glide

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Environment
import android.os.storage.StorageManager
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.coroutines.experimental.launch
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.lang.reflect.InvocationTargetException
import java.util.*


class GlideImageLoader(private val context: Context, private val mProgress: TextView, private val mImageView: ImageView, private val saved: (String) -> Unit, private val saveError: (String) -> Unit) {

    fun load(url: String?, options: RequestOptions?) {
        if (url == null || options == null) return

        onConnecting()

        //set Listener & start
        ProgressAppGlideModule.expect(url, object : ProgressAppGlideModule.UIonProgressListener {
            override fun onProgress(bytesRead: Long, expectedLength: Long) {
                val kb = bytesRead.toInt() / 1024
                mProgress.text = kb.toString() + " KB progression"

            }

            override fun getGranualityPercentage(): Float {
                return 1.0f
            }
        })

        //Get Image
        Glide.with(mImageView.context)
                .load(url)
                .transition(withCrossFade())
                .apply(options.skipMemoryCache(true))
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                        ProgressAppGlideModule.forget(url)
                        onFinished()
                        return false
                    }

                    override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                        ProgressAppGlideModule.forget(url)
                        onFinished()
                        mProgress.text = "Completed."
                        saveImageToSdCard(context, resource, true)

                        return false
                    }
                })
                .into(mImageView)
    }

    private fun saveImageToSdCard(context: Context, resource: Drawable, isRemovable: Boolean) {

        launch {
            val bitmap = (resource as BitmapDrawable).bitmap

            val currentTime = Date().time.toString()
            val dir = File(getStoragePath(context, isRemovable) + "/Android/data/com.app.lineinterview5")
            var file: File? = null
            try {
                dir.mkdirs()

                file = File(dir, "$currentTime.jpg")

                val outStream = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream)
                outStream.flush()
                outStream.close()
                saved(file.toString())
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
                saveError(file.toString())
                saveImageToSdCard(context, resource, false)
            } catch (e: IOException) {
                e.printStackTrace()
                saveError(file.toString())
                saveImageToSdCard(context, resource, false)
            }

        }
    }


    private fun getStoragePath(context: Context, isRemovable: Boolean): String {
        val storageManager = context.getSystemService(Context.STORAGE_SERVICE) as StorageManager

        val storageVolumeClazz: Class<*>?
        try {
            storageVolumeClazz = Class.forName("android.os.storage.StorageVolume")
            val getVolumeList = storageManager.javaClass.getMethod("getVolumeList")
            val getPath = storageVolumeClazz!!.getMethod("getPath")
            val isRemovableMtd = storageVolumeClazz.getMethod("isRemovable")
            val result = getVolumeList.invoke(storageManager)
            val length = java.lang.reflect.Array.getLength(result)
            for (i in 0 until length) {
                val storageVolumeElement = java.lang.reflect.Array.get(result, i)
                val path = getPath.invoke(storageVolumeElement) as String
                val removable = isRemovableMtd.invoke(storageVolumeElement) as Boolean
                if (removable == isRemovable) {
                    return path
                }
            }
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }

        return Environment.getExternalStorageDirectory().absolutePath
    }

    private fun onConnecting() {
        mProgress.visibility = View.VISIBLE
    }


    private fun onFinished() {
        mImageView.visibility = View.VISIBLE
    }

}
