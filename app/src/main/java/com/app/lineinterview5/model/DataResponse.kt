package com.app.lineinterview5.model

data class DataResponse(
        var title: String = "",
        var cover_images: MutableList<String> = mutableListOf()
)