/*
 * Copyright 2015, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.app.lineinterview5

import android.app.Activity
import com.app.lineinterview5.model.DataResponse
import com.toyota.toyota.repository.RepositoryContract

/**
 * Listens to user actions from the UI ([LoginActivity]), retrieves the data and updates
 * the UI as required.
 */
class MainPresenter(
        private val mainView: MainContract.View,
        private val mainRepository: RepositoryContract
) : MainContract.UserActionsListener {

    override fun onDownloadInitialData(activity: Activity) {

        mainView.showLoading()

        mainRepository.downloadData(activity, object: RepositoryContract.MainCallback{

            override fun responseSuccess(dataResponse: DataResponse) {
                mainView.dismissLoading()
                mainView.showResponseSuccess(dataResponse)
            }

            override fun responseError() {
                mainView.dismissLoading()
                mainView.showResponseError()

            }

        })
    }
    override fun onClickDownloadNextImage(lastImage: Int, coverImages: MutableList<String>) {

        var nextImage = 0

        if (lastImage < coverImages.size - 1) {
            nextImage = lastImage + 1
            mainView.showDownloadButton(nextImage)

        }

        if (nextImage == coverImages.lastIndex) {
            mainView.showCompletedButton()
        }


    }

}
