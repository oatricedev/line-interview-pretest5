/*
 * Copyright 2015, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.app.lineinterview5

import android.app.Activity
import com.app.lineinterview5.model.DataResponse

/**
 * This specifies the contract between the view and the presenter.
 */
interface MainContract {

    interface View {

        fun showLoading()

        fun dismissLoading()

        fun showResponseSuccess(dataResponse: DataResponse)

        fun showDownloadButton(lastImage: Int)

        fun showCompletedButton()

        fun showImageProgression()

        fun showResponseError()

    }

    interface UserActionsListener {

        fun onDownloadInitialData(activity: Activity)

        fun onClickDownloadNextImage(lastImage: Int, cover_images: MutableList<String>)

    }
}
