package com.app.lineinterview5

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.lineinterview5.utils.glide.GlideImageLoader
import com.bumptech.glide.Priority
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item.view.*



class Adapter(private val saved: (String) -> Unit, private val saveError: (String) -> Unit) :  RecyclerView.Adapter<Adapter.MyViewHolder>() {

    private var dataList: MutableList<String> = arrayListOf()

    override fun getItemCount(): Int {
        return dataList.size

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(dataList[position])
    }

    fun addData(url: String) {
        dataList.add(url)
        notifyItemInserted(dataList.size)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(url: String) = with(itemView) {

            val options = RequestOptions()
                    .placeholder(R.drawable.ic_placeholder)
                    .priority(Priority.HIGH)

            GlideImageLoader(context, tvMessage, ivImage, saved, saveError).load(url, options)
        }
    }
}