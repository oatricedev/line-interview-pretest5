package com.app.lineinterview5.repository

import android.app.Activity
import com.app.lineinterview5.model.DataResponse
import com.app.lineinterview5.R
import com.app.lineinterview5.utils.getJson
import com.toyota.toyota.repository.RepositoryContract
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch

/**
 * Concrete implementation to load notes from the a data source.
 */
class MainRepository : RepositoryContract {

    override fun downloadData(activity: Activity, callback: RepositoryContract.MainCallback) {

        launch {
            val dataResponse = async { fakeDownloadData(activity) }
            callback.responseSuccess(dataResponse.await())

        }
    }

    suspend fun fakeDownloadData(activity: Activity): DataResponse {
        delay(3000L)
        return activity.getJson(R.raw.data)
    }

}
