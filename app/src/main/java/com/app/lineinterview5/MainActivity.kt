package com.app.lineinterview5

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.app.lineinterview5.model.DataResponse
import com.app.lineinterview5.repository.MainRepository
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity(), MainContract.View {

    var lastImage = 0
    private var adapter: Adapter? = null
    private var dataResponse = DataResponse()
    private lateinit var mActionListener: MainPresenter
    private val REQUEST_STORAGE_PERMISSION = 1001

    override fun showLoading() {
        Toast.makeText(this, "Start Loading...", Toast.LENGTH_SHORT).show()
        
    }

    override fun dismissLoading() {
        runOnUiThread {
            Toast.makeText(this, "Stop Loading", Toast.LENGTH_SHORT).show()
        }
    }

    override fun showResponseSuccess(dataResponse: DataResponse) {

        runOnUiThread {
            this.dataResponse = dataResponse

            adapter = Adapter(
                    { path ->
                        runOnUiThread {
                            Toast.makeText(this, "Success Saved to $path.", Toast.LENGTH_SHORT).show()
                        }
                    },
                    { path ->
                        runOnUiThread {
                            Toast.makeText(this, "Error Save to $path.", Toast.LENGTH_SHORT).show()
                        }
                    })

            tvTitle.text = dataResponse.title

            recyclerview.layoutManager = LinearLayoutManager(this@MainActivity)
            recyclerview.adapter = adapter

            if (dataResponse.cover_images.isNotEmpty()) {
                adapter?.addData(dataResponse.cover_images[0])
            }
        }
    }

    override fun showDownloadButton(lastImage: Int) {
        this.lastImage = lastImage
        adapter?.addData(dataResponse.cover_images[lastImage])
        recyclerview.scrollToPosition(lastImage)
        
    }

    override fun showCompletedButton() {
        tvDownloadImage.text = "Completed."
        
    }

    override fun showImageProgression() {
        
    }

    override fun showResponseError() {
        Toast.makeText(this, "Error!!!", Toast.LENGTH_SHORT).show()
        
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startRequestPermission()
    }

    private fun startApp() {
        mActionListener = MainPresenter(this, MainRepository())

        mActionListener.onDownloadInitialData(this)

        tvDownloadImage.setOnClickListener {
            mActionListener.onClickDownloadNextImage(lastImage, dataResponse.cover_images)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.none { data -> data == 0 }) {
            Toast.makeText(this, "กรุณาให้สิทธิ์ในการเก็บข้อมูลลง SD card.", Toast.LENGTH_LONG).show()
            finish()

        } else {
            startApp()
        }
    }

    private fun startRequestPermission() {

        if (Build.VERSION.SDK_INT >= 23) {
            askForPermissions(REQUEST_STORAGE_PERMISSION)
        } else {
            startApp()
        }
    }

    private fun askForPermissions(requestCode: Int) {
        val permissionsToRequest = arePermissionsGranted()

        if (!permissionsToRequest.isEmpty()) {
            ActivityCompat.requestPermissions(this, permissionsToRequest.toTypedArray(), requestCode)

        } else {
            startApp()
        }
    }

    private fun arePermissionsGranted(): ArrayList<String> {
        val permissions = arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE)

        val permissionsToRequest = ArrayList<String>()
        permissions.filterTo(permissionsToRequest) {
            ActivityCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED
        }

        return permissionsToRequest
    }

    private fun aaa() {
        bbb({ path ->
            return@bbb "aaa"
        })
    }

    private fun bbb(b: (String) -> String) {
        val c = b("aaa")
    }
}
